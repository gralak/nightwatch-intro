SELENIUM_JAR            = selenium-server-standalone-3.0.1.jar
SELENIUM_JAR_URL        = http://selenium-release.storage.googleapis.com/3.0/selenium-server-standalone-3.0.1.jar
CHROME_DRIVER           = chromedriver
CHROME_DRIVER_MAC       = chromedriver_mac64.zip
CHROME_DRIVER_MAC_URL   = https://chromedriver.storage.googleapis.com/2.25/chromedriver_mac64.zip
CHROME_DRIVER_LINUX     = chromedriver_linux64.zip
CHROME_DRIVER_LINUX_URL = https://chromedriver.storage.googleapis.com/2.25/chromedriver_linux64.zip
NPM_BIN                 = $(shell npm bin)

ifeq ($(shell uname),Linux)
	PLATFORM=LINUX
else
	PLATFORM=MAC
endif

.PHONY: checkenv install run test

all: install test

checkenv:
    ifeq ($(PLATFORM),)
	    @echo "Unsuported platform: $(shell uname)"
	    exit 1
    endif

install: ./node_modules checkenv $(SELENIUM_JAR) $(CHROME_DRIVER)
	./tools/ensure_selenium.sh $(SELENIUM_JAR)

./node_modules:
	npm install

test:
	$(NPM_BIN)/nightwatch


$(CHROME_DRIVER):
	curl $(CHROME_DRIVER_$(PLATFORM)_URL) > $(CHROME_DRIVER_$(PLATFORM))
	unzip $(CHROME_DRIVER_$(PLATFORM))
	rm $(CHROME_DRIVER_$(PLATFORM))

$(CHROME_DRIVER_LINUX):
	curl $(CHROME_DRIVER_LINUX_URL) > $(@)

$(CHROME_DRIVER_MAC):
	curl $(CHROME_DRIVER_MAC_URL) > $(@)

$(SELENIUM_JAR):
	curl $(SELENIUM_JAR_URL) > $(@)
