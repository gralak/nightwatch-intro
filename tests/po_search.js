module.exports = {
    'Search ten-x.com commercial (use page object)' : function (browser) {
        var page = browser.page.commercial();

        page.navigate();
        page.assert.visible('@searchBox');
        page.setValue('@searchBox', 'los angeles');
        page.click('@searchButton');

    }
};
