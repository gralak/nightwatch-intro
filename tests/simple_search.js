module.exports = {
    'Search ten-x.com commercial' : function (browser) {
        browser.url('http://www.ten-x.com');
        browser.useXpath();
        browser.click('/html/body/div[1]/div/header/div/div/div/nav/ul/li[2]/a');
        browser.waitForElementPresent('//*[@id="content"]/div/div/div[1]/div/div[1]/div/div[4]/form/div/input', 1000);
        browser.setValue('//*[@id="content"]/div/div/div[1]/div/div[1]/div/div[4]/form/div/input', 'los angeles');
        browser.submitForm('//*[@id="content"]/div/div/div[1]/div/div[1]/div/div[4]/form');
    }
};
