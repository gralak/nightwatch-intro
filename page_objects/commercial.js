module.exports = {
    url: 'https://www.ten-x.com/commercial/',
    elements: {
        searchBox: '#content > div > div > div.full-width_index_3RP-sourceMap.section-hero_index_3mS-sourceMap > div > div.fixed-header_index_2Q--sourceMap > div > div.fe-header-search_index_2Vf-sourceMap > form > div > input',
        searchButton: '#content > div > div > div.full-width_index_3RP-sourceMap.section-hero_index_3mS-sourceMap > div > div.fixed-header_index_2Q--sourceMap > div > div.fe-header-search_index_2Vf-sourceMap > form > div > button',
        searchForm: '#content > div > div > div.full-width_index_3RP-sourceMap.section-hero_index_3mS-sourceMap > div > div.fixed-header_index_2Q--sourceMap > div > div.fe-header-search_index_2Vf-sourceMap > form'
    },
};
