#!/bin/bash

SELENIUM_JAR=$1
SELENIUM_PORT=4444
SELENIUM_LOG=${SELENIUM_JAR}.log
SELENIUM_PID=${SELENIUM_JAR}.pid

function start_selenium {
    java -jar $SELENIUM_JAR >> $SELENIUM_LOG 2>&1 &
    echo $! > $SELENIUM_PID
}

function check_selenium {
    curl -s http://localhost:$SELENIUM_PORT > /dev/null
    return $?
}

check_selenium
if [ "$?" -eq "0" ]; then
   exit 0
fi

start_selenium

POLL=TRUE
ATTEMPTS=0
while [ "$POLL" == "TRUE" ]; do
    check_selenium

    if [ "$?" -eq "0" ]; then
        POLL=FALSE
    elif [[ "$ATTEMPTS" -gt 60 ]]; then
        echo "Failed to start $SELENIUM_SERVER"
        exit 1
    else
        ATTEMPTS=$((ATTEMPTS+1))
        sleep 1
    fi
done

echo "Selenium server is now running...check logs in $SELENIUM_LOG"

