var util = require('util');
var events = require('events');

function Type() {
    events.EventEmitter.call(this);
}

util.inherits(Type, events.EventEmitter);

Type.prototype.command = function(selector, text) {
    this.keyDelay = 2000;
    this.selector = selector;
    this.text = text;
    this.idx = 0;

    this.pressNextKey().then(this.afterPress.bind(this));
    return this;
};

Type.prototype.pressNextKey = function() {
    return new Promise(function (resolve, reject) {
        this.client.api.setValue(this.selector, this.text[this.idx], resolve);
    }.bind(this));
};

Type.prototype.afterPress = function() {
    this.idx = this.idx + 1;
    if (this.idx >= text.length) {
        self.emit('complete');
    } else {
        setTimeout(function () {
            this.pressNextKey().then(this.afterPress.bind(this));
        }.bind(this), this.keyDelay);
    }

};

module.exports = Type;
